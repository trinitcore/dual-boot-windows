﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DualBootAndroid
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Process diskpart = new Process();
            diskpart.StartInfo.UseShellExecute = false;
            diskpart.StartInfo.RedirectStandardOutput = true;
            diskpart.StartInfo.CreateNoWindow = true;
            diskpart.StartInfo.FileName = @"C:\Windows\System32\diskpart.exe";
            diskpart.StartInfo.RedirectStandardInput = true;
            diskpart.Start();
            diskpart.StandardInput.WriteLine("select disk 0");
            diskpart.StandardInput.WriteLine("select part 1");
            diskpart.StandardInput.WriteLine("assign letter = z");
            diskpart.StandardInput.WriteLine("exit");
            string output = diskpart.StandardOutput.ReadToEnd();
            diskpart.WaitForExit();

            Process renameFile = new Process();
            renameFile.StartInfo.UseShellExecute = false;
            renameFile.StartInfo.CreateNoWindow = true;
            renameFile.StartInfo.RedirectStandardOutput = true;
            renameFile.StartInfo.RedirectStandardInput = true;
            renameFile.StartInfo.FileName = @"C:\Windows\System32\cmd.exe";
            renameFile.Start();
            renameFile.StandardInput.WriteLine(@"ren Z:\EFI\Microsoft\Boot\bootmgfw.efi bootmgfw_1.efi");
            renameFile.StandardInput.WriteLine(@"exit");
            output += renameFile.StandardOutput.ReadToEnd();
            renameFile.WaitForExit();

            Console.WriteLine(output);

            Process restart = new Process();
            restart.StartInfo.FileName = "shutdown.exe";
            restart.StartInfo.Arguments = "-r -t 0";
            restart.StartInfo.UseShellExecute = false;
            restart.StartInfo.CreateNoWindow = true;
            restart.Start();

            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
        }
    }
}
